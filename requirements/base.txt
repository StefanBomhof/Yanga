# core apps
Django==1.11.8
wagtail==1.13.1

# less core apps
wagtailfontawesome==1.1.1
wagtailmenus==2.5.2
wagtail-metadata==0.3.1
django-compressor==2.2

# secured, from gitlab
git+ssh://git@gitlab.com/uwkm-frets/RoadRunner.git
git+ssh://git@gitlab.com/uwkm-frets/streamfields.git
git+ssh://git@gitlab.com/uwkm-frets/pages.git

# last updated 16/02/2018 14:40
