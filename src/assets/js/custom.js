$(document).ready(function(){
	
	$('.section').imagesLoaded( { background: true }, function() {
		$('.dots-anime').removeClass('active');
		$('#dot-1').addClass('active');
		setTimeout(function(){
			$('.overlay').slideUp();
			setTimeout(function(){
				$('.loader').hide();
			}, 200);

			anime.timeline()
			.add({
				targets: '#section-1 .fp-tableCell',
				opacity: 1,
				duration: 10,
			});

			anime({
				targets: '.logo-header',
				translateY: [-200, 0],
				opacity: {
					value: [0, 1],
				},
				duration: 2500,
			});

			anime({
				targets: '#section-1',
				backgroundPosition: ['0% 0%', '0% 100%'],
  				duration: 3000,
  				loop: true,
			});
			anime.timeline()
			.add({
				targets: ['.dash-header','#water-logo'],
				translateY: ['-20vh','0vh'],
				duration: 1000,
				easing: 'easeOutExpo',		  	
			})
			.add({
				targets: '.tile',
				opacity: [0,1],
				duration: 1600,
				delay: function(div, i, l) {
				    return i * 100;
		  		},
						  	
			});
		}, 500)
	});

	$('#fullpage').fullpage({
		lazyLoading: false,
		parallax: true,
		parallaxOptions: { type: 'reveal', percentage: 40, property: 'translate'},
		anchors: ['section1', 'section2', 'section3', 'section4', 'section5'],
		onLeave: function(index, nextIndex, direction){
			if(nextIndex == 1 ){
				$('.dots-anime').removeClass('active');
				$('#dot-1').addClass('active');
        		setTimeout(function(){
						anime({
						    targets: ['#fruit1', '#fruit5'],
						    translateY: [0,-1000],
						    easing: "easeInOutQuad",
						    duration: 600
						});
						anime({
						    targets: ['#fruit2', '#fruit8'],
						    translateY: [0,1000],
						    easing: "easeInOutQuad",
						    duration: 600
						});
						anime({
						    targets: '#fruit3',
						    translateY: [0,-1000],
						    easing: "easeInOutQuad",
						    duration: 600
						});
						anime({
						    targets: '#fruit7',
						    translateY: [0,-450],
						    easing: "easeInOutQuad",
						    duration: 600
						});
						anime({
						    targets: ['#fruit4', '#fruit6'],
						    translateY: [0,500],
						    easing: "easeInOutQuad",
						    duration: 600
						});
					}, 50);
			}

			if(nextIndex == 2 ){
				$('.dots-anime').removeClass('active');
				$('#dot-2').addClass('active');
					$('#section-2 .title .letters').each(function(){
					  $(this).html($(this).text().replace(/([^\x00-\x80]|\w|-)/g, "<span class='letter'>$&</span>"));
					});
				setTimeout(function(){
					if($('#section-2').hasClass('firstvisit')){
						$('#section-2').removeClass('firstvisit');
						anime.timeline()
						.add({
							targets: '#section-2 .list-wrapper ul li',
							translateX: ['50vw', '0vw'],
					  		direction: 'alternate',
					  		duration: 1500,
						  	delay: function(div, i, l) {
							    return i * 80;
					  		},
						});
					}
					anime({
						targets: '#section-2 .fp-tableCell',
						opacity: 1,
						duration: 10,
					});
					anime({
					    targets: ['#fruit1', '#fruit5'],
					    translateY: [-1000,0],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit2',
					    translateY: [200,0],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit8',
					    translateY: [800,0],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit3',
					    translateY: [-1000,0],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit7',
					    translateY: [-450,0],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: ['#fruit4', '#fruit6'],
					    translateY: [500,0],
					    easing: "easeInOutQuad",
					    duration: 600
					});
				}, 50);
		  	}
		  	if(nextIndex == 3 ){
				$('.dots-anime').removeClass('active');
				$('#dot-3').addClass('active');
				if($('#section-3').hasClass('firstvisit')){
					$('#section-3').removeClass('firstvisit');
					anime.timeline()
					.add({
						targets: ['.white-circle', '#section-3 .list-wrapper ul li'],
						translateX: ['50vw', '0vw'],
						scale: [0,1],
				  		direction: 'alternate',
				  		duration: 1000,
				  		easing: 'easeOutBack',
					  	delay: function(div, i, l) {
						    return i * 80;
				  		},
					});
				}
				setTimeout(function(){
					anime({
					    targets: ['#fruit1', '#fruit5'],
					    translateY: [0,-1000],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit2',
					    translateY: [0,200],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit8',
					    translateY: [0,800],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit3',
					    translateY: [0,-1000],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: '#fruit7',
					    translateY: [0,-850],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
					    targets: ['#fruit4', '#fruit6'],
					    translateY: [0,500],
					    easing: "easeInOutQuad",
					    duration: 600
					});
					anime({
						targets: '#annanas',
						translateX: {
							value: [0, -500],
							duration: 1200,
							easing: 'easeOutExpo',
						},
						rotate: {
							value: [0, 90],
							duration: 1200,
							easing: 'easeOutExpo',
						},
					});
					anime({
						targets: '#annanas_slice',
						translateX: {
							value: [-0, -400],
							duration: 1200,
							easing: 'easeOutExpo',
						},
						rotate: {
							value: [0, 90],
							duration: 1200,
							easing: 'easeOutExpo',
						},
					});
				}, 150);
			}
			if(nextIndex == 4 ){
				$('.dots-anime').removeClass('active');
				$('#dot-4').addClass('active');
				if($('#section-4').hasClass('firstvisit')){
					$('#section-4').removeClass('firstvisit');
					setTimeout(function(){
						anime.timeline()
						.add({
							targets: '#section-4 .content-hide',
							opacity: 1,
							duration: 10,
						})
						.add({
							targets: '.header-animate',
							translateY: ['-50vh', '0vh'],
							easing: 'easeOutBack',
							duration: 750,
						})
						.add({
							targets: '.link-list ul li',
							translateX: ['100vw', '0vw'],
					  		direction: 'alternate',
					  		duration: 1500,
						  	delay: function(div, i, l) {
							    return i * 50;
					  		},
						});
					}, 500);
				}
				setTimeout(function(){
					anime({
						targets: '#annanas',
						translateX: {
							value: ['-50vw', '0vw'],
							duration: 800,
							easing: 'easeOutExpo',
						},
						rotate: {
							value: [90, 0],
							duration: 800,
							easing: 'easeOutExpo',
						},
					});
					anime({
						targets: '#annanas_slice',
						translateX: {
							value: [-400, 0],
							duration: 800,
							easing: 'easeOutExpo',
						},
						rotate: {
							value: [90, 0],
							duration: 800,
							easing: 'easeOutExpo',
						},
					});
				}, 150);
			}
		}

	});
	$('.members-only').on('click', function(){
		if($('.login-hide').hasClass('active')){
			$('.login-hide').removeClass('active');
			$('.members-only').removeClass('active');
			$('.overlay-login').removeClass('active');
		}else{
			setTimeout(function(){
				$('.login-hide').addClass('active');
				$('.members-only').addClass('active');
				$('.overlay-login').addClass('active');
			}, 10);
			$('.login-hide').removeClass('d-none');
		}
	});

	$('.overlay-login').on('click', function(){
		$('.login-hide').removeClass('active');
		$('.members-only').removeClass('active');
		$('.overlay-login').removeClass('active');
		setTimeout(function(){
			$('.login-hide').addClass('d-none');
		}, 500);
	});
});




