from django.db import models

from wagtail.wagtailcore.models import Page
from wagtailmetadata.models import MetadataPageMixin
from wagtail.wagtailcore.fields import StreamField
from wagtail.wagtailcore import blocks
from wagtail.contrib.settings.models import BaseSetting, register_setting
from wagtail.wagtailadmin.edit_handlers import FieldPanel, MultiFieldPanel, StreamFieldPanel
from wagtail.wagtailimages.blocks import ImageChooserBlock

from kwiktail.blocks import get_streamfields, register
from pages.models import AbstractStreamfieldPage
from roadrunner.edit_handler import UwkmStreamFieldPanel

COLOR_CHOICES = (
    ("light", "Wit"),
    ("dark", "Zwart"),
)

class HomePage(MetadataPageMixin, Page):
    template = 'pages/home.html'


class Dashboard(MetadataPageMixin, Page):
    template = 'pages/dashboard.html'



# Wagtail Settings

@register_setting
class SiteSettings(BaseSetting):
    analytics_id = models.CharField(
        verbose_name="Google Analytics UA code",
        help_text="Begint altijd met UA-",
        max_length=60,
        blank=True,
    )
    geo_placename = models.CharField(
        verbose_name="GEO plaatsnaam",
        help_text="Bijvoorbeeld: Deventer",
        max_length=255,
        blank=True,
    )
    geo_position = models.CharField(
        verbose_name="GEO positie",
        help_text="Bijvoorbeeld: 52.240672;6.199859",
        max_length=255,
        blank=True,
    )
    geo_region = models.CharField(
        verbose_name="GEO regio",
        help_text="Bijvoorbeeld: NL",
        max_length=255,
        blank=True,
    )
    cm_main_list = models.CharField(
        verbose_name='CM hoofdlijst',
        help_text = 'Hoofd lijst van Campaign Monitor, hier worden'
            'standaard alle leden aan toegevoegd.',
        max_length = 255,
        blank = True,
    )
    google_site_verification = models.CharField(
        max_length=255,
        blank=True,
    )
    alexa_verify_id = models.CharField(
        max_length=255,
        blank=True,
    )
    content = models.TextField(
        verbose_name="Footer inhoud",
        help_text="Dit is de inhoud van je footer.",
        blank=True,
    )
    panels = [
        FieldPanel('cm_main_list'),
        MultiFieldPanel(
            [
                FieldPanel('analytics_id'),
                FieldPanel('geo_placename'),
                FieldPanel('geo_position'),
                FieldPanel('geo_region'),
                FieldPanel('google_site_verification'),
                FieldPanel('alexa_verify_id'),
            ],
            heading="SEO Meta tags",
            classname="collapsible"
        ),
        MultiFieldPanel(
            [
                FieldPanel('content'),
            ],
            heading="Footer",
            classname="collapsible"
        ),
    ]

    class Meta:
        verbose_name = "Instellingen"

    def coordinates(self):
        return self.geo_position.split(';')

    @property
    def get_icbm(self):
        return ', '.join(self.coordinates())
        