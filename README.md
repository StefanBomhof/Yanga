UWKM Basis installatie
======================

Naam wijzigen: python manage.py rename_project *nieuwe_naam*

Stappen:
- git clone git@gitlab.com:uwkm/basis-installatie.git project_naam
- cd project_naam
- maak een nieuw project op gitlab: https://gitlab.com/projects/new

```shell
git remote remove origin
git remote add origin git@gitlab.com:uwkm/project_naam.git
```

- cd requirements
- mkvirtualenv project_naam
- pip install -r dev.txt
- cd ../src/base_install/settings/
- touch debug.py
- mv local_example.py local.py
- Voeg een `SECRET_KEY` toe, nieuwe generen: `python manage.py secret_key`
- cd ../../
- ./manage.py rename_project project_naam
- ./manage.py migrate
- ./manage.py createsuperuser
- ./manage.py runserver
- log in op de admin
- voeg nieuwe pagina's toe
- voeg content aan de homepagina toe
- voeg content aan de footer toe (Settings > footer)
- verander de settings van de streamfields (Settings > Streamfields settings)
- voeg menu items toe (Settings > main menu)
 - in deze installatie zit alleen nog maar main menu support
 - zorg ervoor dat je in de pagina's bij "Promote" het vinkje aanstaat "Show in menus"
 - voor dropdown menus klik op advanced settings en selecteer bij maximum levels: 4
 - voeg alle menu items toe (vraag hulp aan @Temoeri)

- Als laatste pushen we de wijzigingen naar git

```shell
git push -u origin --all
git push -u origin --tags
```

UWKM
